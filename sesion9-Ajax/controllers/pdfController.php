<?php

class pdfController extends  Controller {


	private $_pdf;

	public function __construct(){
		parent::__construct();
		$this->getLibrary('fpdf');
		$this->_pdf = new FPDF;

	}

	public function index(){

		$this->_pdf->AddPage();
		$this->_pdf->SetFont('Arial','B',16);
		$this->_pdf->Cell(40,10,'Hola Mundo');
		$this->_pdf->Output();

	}

	public function index2(){


		$this->_pdf->AliasNbPages();
		$this->_pdf->AddPage();
		$this->_pdf->SetFont('Times','',12);
		for($i=1;$i<=40;$i++)
		$this->_pdf->Cell(0,10,'Imprimiendo l�nea n�mero '.$i,0,1);
		$this->_pdf->Output();


	}

	public function index3(){

		// define la cabecera de la tabla
		$header = array('Pa�s', 'Capital', 'Superficie (km2)', 'Pobl. (en miles)');

		$data = array();
		// Carga de datos
		$data[] = array ('Austria','Vienna',838590,8075);
		$data[] = array ('Belgium','Brussels',30518,10192);
		$data[] = array ('Denmark','Copenhagen',43094,5295);
		$data[] = array ('Finland','Helsinki',304529,5147);
		$data[] = array ('France','Paris',543965,58728);
		$data[] = array ('Germany','Berlin',357022,82057);
		$data[] = array ('Ireland','Dublin',70723,3694);
		$data[] = array ('Italy','Roma',301316,57563);
		$data[] = array ('Luxembourg','Luxembourg',2586,424);
		$data[] = array ('Netherlands','Amsterdam',41526,15654);

		// define tipo de letra
		$this->_pdf->SetFont('Arial','',14);
		$this->_pdf->AddPage();

		foreach($header as $col)
		$this->_pdf->Cell(40,7,$col,1);

		$this->_pdf->Ln();
		 
		// Datos
		foreach($data as $row)
	 	{
	 		foreach($row as $col)
	 		$this->_pdf->Cell(40,6,$col,1);
	 		$this->_pdf->Ln();
	 	}


		$this->_pdf->AddPage();
		$w = array(40, 35, 45, 40);
		// Cabeceras
		for($i=0;$i<count($header);$i++)
			$this->_pdf->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->_pdf->Ln();
		// Datos
		foreach($data as $row)
		{
			$this->_pdf->Cell($w[0],6,$row[0],'LR');
			$this->_pdf->Cell($w[1],6,$row[1],'LR');
			$this->_pdf->Cell($w[2],6,number_format($row[2]),'LR',0,'R');
			$this->_pdf->Cell($w[3],6,number_format($row[3]),'LR',0,'R');
			$this->_pdf->Ln();
		}
		// L�nea de cierre
		$this->_pdf->Cell(array_sum($w),0,'','T');
	
		$this->_pdf->Output();

	}



}


?>