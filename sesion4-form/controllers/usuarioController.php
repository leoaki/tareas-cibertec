<?php

class usuarioController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}
	
	public function index()
	{
	$this->_view->titulo = 'Usuarios';
    $this->_usuario = $this->loadModel('usuario');
    $this->_view->usuarios= $this->_usuario->getAllUsuario();
    
	$this->_view->renderizar('index');
			
	}
	
	public function addUsuario(){
	// recepcion de datos del formulario  y limpieza de los espacios en blanco
	$usuario = trim($_POST['usuario']);
	$nombre = trim($_POST['nombre']);
	$contrasenia = trim($_POST['contrasenia']);
	
	// carga el modelo y llama al metodo para insertar un usuario
	$this->_usuario = $this->loadModel('usuario');
	$result = $this->_usuario->setUsuario($usuario,$nombre,$contrasenia);

	// validacion del error al grabar
	if ($result)  $this->_view->error ='Error al registrar usuario'; 	
	
	$this->_view->renderizar('index');
	}
	
	public function deleteUsuario($id){
		$this->_view->titulo = 'Eliminar usuarios';
	    $this->usuario = $this->loadModel('usuario');
	    $result= $this->usuario->DeleteUser($id);
	    if($result) $this->_view->error='Error al eliminar';
	    
		$this->redireccionar('usuario/index');			
	}
	public function editarUsuario($id){
                $this->_model = $this->loadModel('usuario');
                $user = $this->_model->existeUsuario($id);
                if($user){
                        $this->_view->usuario = $user;
                }else{
                        $this->_view->error = 'No existe el usuario';
                }
                $this->_view->renderizar('editar');
        }

}