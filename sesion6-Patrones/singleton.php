<?php 

class Unica
{
    private static $instancia;
    private $count = 0;
    
 
    public static function singleton() 
    {
        if (!isset(self::$instancia)) {
            echo 'Creando nueva instancia <br>';
            $nombreClase = __CLASS__;
            self::$instancia = new $nombreClase;
        }
        return self::$instancia;
    }

    public function incremento()
    {
        echo $this->count++;
    }


}


$singleton1 = Unica::singleton(); // imprime "Creando nueva instancia."
echo $singleton1->incremento().'<br>'; // 0
echo $singleton1->incremento().'<br>'; // 1

$singleton2 = Unica::singleton(); // reutiliza la instancia ya existente
echo $singleton2->incremento().'<br>'; // 2
echo $singleton2->incremento().'<br>'; // 3


?>


