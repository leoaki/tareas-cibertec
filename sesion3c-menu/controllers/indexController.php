<?php

class indexController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}

	public function index()
	{
		$this->_view->titulo = 'Portada';


		$this->_view->renderizar('index');
	}

	public function login()
	{
		$this->_view->titulo = 'login';

		if (isset($_POST['usuario']) && isset($_POST['contrasenia'])){
			$usuario = trim($_POST['usuario']);
			$contrasenia = trim($_POST['contrasenia']);
			
			$this->_usuario = $this->loadModel('usuario');
			$result = $this->_usuario->existeUsuario($usuario,$contrasenia);
			
			if ($result) $_SESSION['usuario_app01'] = $usuario;
			
		}
		$this->_view->renderizar('index');
	}
	
	public function logout(){
		unset($_SESSION['usuario_app01']);		
		$this->_view->renderizar('index');
	}
}

?>