<?php

/*
 * -------------------------------------
 *  Config.php
 * -------------------------------------
 */

define('BASE_URL', 'https://192.168.0.111:8080/');
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_LAYOUT', 'default');

define('DB_HOST', 'localhost');
define('DB_USER', 'usertienda');
define('DB_PASS', '12345');
define('DB_NAME', 'tienda2');
define('LIMIT_REGXPAG',5);
define('LIMIT_PAGEVIEW',10);
define('DB_ENGINE','mysql');
define('DB_CHAR', 'utf8');

?>
