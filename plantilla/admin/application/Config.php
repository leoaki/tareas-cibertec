<?php

/*
 * -------------------------------------
 *  Config.php
 * -------------------------------------
 */


define('BASE_URL', 'http://192.168.1.20/admin/');
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_LAYOUT', 'default');

define('APP_NAME', 'Mi blog');
define('APP_SLOGAN', 'mi primer framework php y mvc...');
define('APP_COMPANY', 'www.proyectos.com');

define('DB_HOST', 'localhost');
define('DB_USER', 'usermvc');
define('DB_PASS', '12345');
define('DB_NAME', 'mvc');
define('DB_CHAR', 'utf8');

?>
