<?php
require_once 'config.php';
require_once __DIR__. '/php-activerecord/ActiveRecord.php';

class Autor extends ActiveRecord\Model { }

//instancia
$cfg = ActiveRecord\Config::instance();
$cfg->set_model_directory('.');
//$cfg->set_connections(array('development' => 'mysql://userlibreria:12345@localhost/libreria'));
$cfg->set_connections(array('development' => DB_ENGINE.'://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME));

//guardar el autor 1
$autor1 = new Autor(array('id'=>1,'nombre' => 'Mario Vargas LLosa'));
$autor1->save();

//guardar el autor 2
$autor2 = new Autor(array('id'=>2,'nombre' => 'Gabriel Garcia Marquez'));
$autor2->save();

//guardar el autor 3
$autor2 = new Autor(array('id'=>3,'nombre' => 'Alfredo Bryce Echenique'));
$autor2->save();

echo 'registros grabados';


?>
