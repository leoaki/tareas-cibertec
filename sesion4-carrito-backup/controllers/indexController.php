<?php

class indexController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}

	public function index()
	{
		$this->_view->titulo = 'Tienda';
			
		$this->_producto= $this->loadModel('producto');
		$this->_view->productos = $this->_producto->getProductos();

		$this->_view->renderizar('index');
	}

	public function verFoto($idproducto){
		$this->_producto= $this->loadModel('producto');
		$this->_view->producto = $this->_producto->getProductoPorID($idproducto);
		header("Content-type: ".$this->_view->producto->MIME);
		echo $this->_view->producto->FOTO;
	}

	public function detalle($idproducto){
		$this->_view->titulo = 'Detalle';
		$this->_producto= $this->loadModel('producto');
		$this->_view->producto = $this->_producto->getProductoPorID($idproducto);

		$this->_view->renderizar('detalle');
	}



	public  function cargarCarro(){
		$this->_producto= $this->loadModel('producto');

		$this->_view->importetotal=0;
		$this->_view->cantidadtotal=0;

	
			foreach ($_SESSION['carro'] as $idproducto =>$cantidad){

				$producto= $this->_producto->getProductoPorID($idproducto);
				$this->_view->arrayCarro[$idproducto]['cantidad']=$cantidad;
				$this->_view->arrayCarro[$idproducto]['precio']=$producto->PRECIO;
				$this->_view->arrayCarro[$idproducto]['descripcion']=$producto->DESCRIPCION;
				$this->_view->arrayCarro[$idproducto]['importe']=$producto->PRECIO*$cantidad;
				$this->_view->importetotal+= $this->_view->arrayCarro[$idproducto]['importe'];
				$this->_view->cantidadtotal+=$cantidad;
			}

		$_SESSION['totalImporte']=$this->_view->importetotal;
		$_SESSION['cantidadTotal']=$this->_view->cantidadtotal;



	}


	public function carro($idproducto=null,$action=''){

		$this->_view->titulo = 'Carro';
			
		if ($idproducto != null){

			switch($action){
				case 'add':
					if (isset($_SESSION['carro'][$idproducto])) $_SESSION['carro'][$idproducto]++;
					else
					$_SESSION['carro'][$idproducto]=1;
					break;

				case 'remove':
					if ($_SESSION['carro'][$idproducto]==1) unset($_SESSION['carro'][$idproducto]);
					else $_SESSION['carro'][$idproducto]--;
					break;

				case 'removeProd':  unset($_SESSION['carro'][$idproducto]); break;
			}
		}

		$this->cargarCarro();

		$this->_view->renderizar('carro');
	}

	public function comprar(){
		$this->_view->titulo = 'Compra';
		$this->cargarCarro();
		$this->_view->renderizar('compra');
	}


	public function exito(){

		unset($_SESSION['carro']);
		unset($_SESSION['cantidadTotal']);
		unset($_SESSION['totalImporte']);
		$this->redireccionar('index/index');
	}

	public function cancelar(){

		$this->_view->msg='Se cancelo la compra';
		$this->_view->renderizar('cancelar');
	}

	public function paypalipn(){
		$this->_venta= $this->loadModel('venta');
		$this->_view->renderizar('cancelar');
	}

}

?>