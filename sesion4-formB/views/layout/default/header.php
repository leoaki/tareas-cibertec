<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= isset($this->titulo)?$this->titulo:''; ?></title>
    <meta charset="<?= DB_CHAR ?>/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>bootstrap.css" />	
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>real_estate.css" />
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/bootstrap.js"></script>
	
	
    <!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<? if(isset($_layoutParams['js']) && count($_layoutParams['js'])): ?>
		<? foreach($_layoutParams['js'] as $layout): ?>
   			<script src="<?= $layout ?>" type="text/javascript"></script>
		<? endforeach; ?>
	<? endif; ?>
	
</head>

<body>

 <div class="container">
		<div class="row"><!-- start header -->
			<div class="span4 logo">
				<a href="#">
				<div class="row">
					<div class="span1">
						<img src="<?= $_layoutParams['ruta_img']?>Home-green-48.png" alt=""/>
					</div>
					<div class="span3">
						<h1><small>Inmobiliaria</small><br /><span>Mi</span>casa</h1>
					</div>
				</div> 
				</a>
			</div>		
			<div class="span4 customer_service pull-right text-right">
				<br />
				<h4>Telf: 4232406</h4>
				<h4><small>24 horas al d�a, 7 dias a la semana</small></h4>
			</div>
		</div><!-- end header -->
		
		<div class="row"><!-- start nav -->
			<div class="span12">
				<div class="navbar">
					<div class="navbar-inner">
						<div class="container">
							<div class="nav-collapse">
							
							<?
							  $menu['Catalogo']= array('Usuario'=>'usuario/index','Producto'=>'producto/index');
							  $menu['Imagenes']= array('Imagen1'=>'imagen1/imagen1', 'Imagen2'=>'imagen2/imagen2');
							  $menu['Salir']='index/logout';
							?>
							
							<? if (isset($_SESSION['usuario_app01'])):?>
							   <ul class="nav">
							   	<? if (isset($menu)): ?>
								   <? foreach( $menu  as $op_h =>$opciones):?>
									    <? if (is_array($opciones)):?>
									     <li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$op_h ?> <b class="caret"></b></a>
												<ul class="dropdown-menu">
												   <? foreach ($opciones as $op_v =>$ruta):?>
													<li><a href="<?= BASE_URL; ?><?=$ruta?>"><?=$op_v?></a></li>
												   <? endforeach;?>
												</ul>
										</li>
										<? else:?>
										<li><a href="<?= BASE_URL; ?><?=$opciones?>" ><?= $op_h?></a>
										<? endif;?>
									<? endforeach;?>
								 <? endif;  // fin del if del menu?>		
							   </ul>
							 <? endif; //  fin del isset?>  
							 
							<? if (!isset($_SESSION['usuario_app01'])):?>
								<ul class="nav pull-right">
									
									<li class="dropdown">
										<a class="dropdown-toggle" href="#" data-toggle="dropdown">Registrate<strong class="caret"></strong></a>
										<div class="dropdown-menu">
											<form action="<?=BASE_URL?>usuario/addUsuario" method="post">
												<input type="text" name="usuario" placeholder="usuario" size="30" />
												<input type="text" name="nombre" placeholder="Nombre completo" size="30" />
												<input type="password" name="contrasenia" placeholder="Contrase�a" size="30" />
												<input type="password" name="rpcontrasenia" placeholder="Repita contrase�a" size="30" />
												
												<input class="btn btn-primary" type="submit" name="commit" value="Registrar" />
											</form>
										</div>
									</li>			
									
									<li class="dropdown">
										<a class="dropdown-toggle" href="#" data-toggle="dropdown">Iniciar sesi�n <strong class="caret"></strong></a>
										<div class="dropdown-menu">
										
											<form action="<?= BASE_URL?>index/login" method="post">
												<input type="text" name="usuario" placeholder="Usuario" size="30" />
												<input type="password" name="contrasenia" placeholder="Contrase�a" size="30" />
																								
												<input class="btn btn-primary" type="submit" name="commit" value="Ingresar" />
											</form>
										
										</div>
									</li>
								</ul>
								<? endif;?>	
							</div><!-- /.nav-collapse -->
						</div>
					</div><!-- /navbar-inner -->
				</div><!-- /navbar -->
			</div>
		</div><!-- end nav -->		
		<? if (isset($this->error)) {echo $this->error ; exit;}?>
