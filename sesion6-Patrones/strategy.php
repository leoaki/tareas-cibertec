<?php
 
interface estrategia{
    public function ordenar($productos);
}
 
class OrdenarPorNombre implements estrategia{
 
    public function ordenar($productos){
        ksort($productos);
        return $productos;
    }
 
}
 
class OrdenarPorPrecio implements estrategia{
 
    public function ordenar($productos){
        asort($productos);
        return $productos;
    }
 
}
 
class Almacen {
 
    private $productos;
 
    public function getProductos(){
        return $this->productos;
    }
 
    public function setProductos($productos){
        $this->productos = $productos;
    }
 
    public function mostrar($estrategia){
    	echo '<pre>';
        print_r($estrategia->ordenar($this->getProductos()));
        echo '</pre>';
    }
 
}



/*  array de productos */
$productos = array('Remera'=>20, 'Campera'=>50, 'Patalon'=>35, 'Gorro'=>10);
 
$a = new Almacen;
$a->setProductos($productos);
$a->mostrar(new OrdenarPorNombre);
$a->mostrar(new OrdenarPorPrecio);
 
?>