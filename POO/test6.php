<?php 

//  ejemplo de reescritura del metodo y final
// define la clase
class Persona {
	
	// define las propiedades
	public $dni;
	public $nombre ;
	public $edad;
	public $talla;
	
	public function __construct($dni,$nombre,$edad,$talla){
		$this->dni = (int)$dni;
		$this->nombre = (string)$nombre;
		$this->edad = (int)$edad;
		$this->talla = (float)$talla;
		return;
	}
	
	public function __destruct(){
		echo 'Es el fin';
		
	}
	
} // fin de la clase



class Empleado extends Persona {
	public $id_essalud;
	
	public function __construct($dni,$nombre,$edad,$talla,$id_essalud){
		parent::__construct($dni,$nombre,$edad,$talla);
		$this->id_essalud=(string)$id_essalud;	
		return;
	}	
	
}

$vendedor = new Empleado('10254786','Juan Perez',32,1.74,'7011061SCMEF005');

?>


<html>
<head>

</head>
<body>
	<table border=1>
		<tr>
			<th>Item</th>
			<th>Valor</th>
		</tr>
		<tr>
			<td>DNI</td>
			<td><?=$vendedor->dni?></td>
		</tr>	
		<tr>
			<td>NOMBRE</td>
			<td><?=$vendedor->nombre?></td>
		</tr>
		<tr>
			<td>EDAD</td>
			<td><?=$vendedor->edad?></td>
		</tr>
		<tr>
			<td>TALLA</td>
			<td><?=$vendedor->talla?></td>
		</tr>
		<tr>
			<td>TALLA</td>
			<td><?=$vendedor->id_essalud?></td>
		</tr>
	</table>
</body>
</html>