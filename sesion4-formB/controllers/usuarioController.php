<?php

class usuarioController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}
	
	public function index($pagina =1)
	{
		$this->_view->titulo ='usuarios';
		$this->_usuario = $this->loadModel('usuario');
		//$this->_view->usuarios : se esta generando una variable para la vista.
		$this->_view->usuarios = $this->_usuario->getAllUsuario();
		
		//agrgado ===========================================
		$this->_view->usuarios = $this->_usuario->getUsuarios($pagina);
			
		$this->_view->param = $this->_usuario->getPaginacion();
		//===================================================
		
		$this->_view->renderizar('index');		
	}
	
	public function addUsuario()
	{
		// recepcion de datos del formulario  y limpieza de los espacios en blanco
		$usuario = trim($_POST['usuario']);
		$nombre = trim($_POST['nombre']);
		$contrasenia = trim($_POST['contrasenia']);
		
		// carga el modelo y llama al metodo para insertar un usuario
		$this->_usuario = $this->loadModel('usuario');
		$result = $this->_usuario->setUsuario($usuario,$nombre,$contrasenia);
	
		// validacion del error al grabar
		if ($result)  $this->_view->error ='Error al registrar usuario'; 	
		
		$this->_view->renderizar('index');
	}
	
	public function eliminar($idusuario)
	{
		$this->_view->titulo ='Eliminacion de Usuario';
		$this->_usuario = $this->loadModel('usuario');
		$result = $this->_usuario->offUsuario($idusuario);
		
		if ($result) $this->_view->error = 'Error al desactivar el usuario';
		
		$this->redireccionar('usuario/index');
	}

}