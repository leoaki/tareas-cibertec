<?php
include_once 'config.php';

/*
class Conexion 
{
	private $dbh;

	public function __construct() {

		$this->dbh =  new PDO(DB_ENGINE.':host='.DB_HOST.';dbname='.DB_NAME,DB_USER,DB_PASS);

		return $this->dbh;
	}
}

//  Instancias 
$con1 = new Conexion();
$con2 = new Conexion();
*/


 class Conexion extends PDO
 {
 		private static $dbh;

 		public static function singlenton() {
 			if (!isset(self::$dbh)) {
 						self::$dbh =  new PDO( DB_ENGINE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
 						}
 			return self::$dbh;
 		}
 }
 

 
//  Instancias 
$con1 = Conexion::singlenton();
$con2 = Conexion::singlenton();

if ($con1 === $con2) echo 'son las mismas';
else echo 'son diferentes';



?>


