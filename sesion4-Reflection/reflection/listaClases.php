<?php
$clases=array();

$d=dir("../clases");//dir-->me permite ver los archivos que existen en un directorio
//la funcion dir genera un objeto; d es un objeto
//metodo del objeto dir: read
while (false !== ($archivo = $d->read())) {
	//los archivos de clases son del tipo class.nombreClase.php
	list($nombre,$extension)=explode(".", $archivo);
	if($extension=='php')
	{
		$clases[]=$nombre;
	}
}
natcasesort($clases);//ordena el array
$clases=array_merge(array("Escoge Clase"), $clases);//une arrays

?>
<!DOCTYPE select PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
Seleccione la clase <br><br>
<form action='listaMetodos.php' method='get'>
<select name='clase'>
<?php foreach ($clases as $options):?>
  <option><?=$options?></option>
<?php endforeach;?>
</select>

<br><br><br><br>

<input type='submit' value='enviar'>

</form>
</body>
</html>