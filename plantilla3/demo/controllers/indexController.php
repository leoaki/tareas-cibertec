<?php

class indexController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}

	public function index()
	{
		$this->_view->titulo = 'Portada';


		$this->_view->renderizar('index');
	}

	public function login()
	{
		$this->_view->titulo = 'login';

		if (isset($_POST['usuario']) && isset($_POST['contrasenia'])){
			$usuario = trim($_POST['usuario']);
			$contrasenia = trim($_POST['contrasenia']);
			
			if ($usuario ==	'admin' && $contrasenia=='12345'){
				$_SESSION['usuario_app01'] = $usuario;
			}
		}
		$this->_view->renderizar('index');
	}
	
	public function logout(){
		unset($_SESSION['usuario_app01']);		
		$this->_view->renderizar('index');
	}
}

?>