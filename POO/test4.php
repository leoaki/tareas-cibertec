<?php 

// define la clase
class Persona {
	
	// define las propiedades
	public $dni;
	public $nombre ;
	public $edad;
	public $talla;
	
	public function __construct($dni,$nombre,$edad,$talla){
		$this->dni = (int)$dni;
		$this->nombre = (string)$nombre;
		$this->edad = (int)$edad;
		$this->talla = (float)$talla;
		return;
	}
	
	public function __destruct(){
		echo 'Es el fin';
	}
	
} // fin de la clase



// instancia el objeto
$sujeto = new Persona('10254786','Juan Perez',32,1.74);


?>

<html>
<head>

</head>
<body>
	<table border=1>
		<tr>
			<th>Item</th>
			<th>Valor</th>
		</tr>
		<tr>
			<td>DNI</td>
			<td><?=$sujeto->dni?></td>
		</tr>	
		<tr>
			<td>NOMBRE</td>
			<td><?=$sujeto->nombre?></td>
		</tr>
		<tr>
			<td>EDAD</td>
			<td><?=$sujeto->edad?></td>
		</tr>
		<tr>
			<td>TALLA</td>
			<td><?=$sujeto->talla?></td>
		</tr>
		
	</table>
</body>
</html>