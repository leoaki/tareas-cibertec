<?php

class productoModel extends Model
{
	public function __construct() {
		parent::__construct();
	}

	public function getProductos($pagina=1)
	{
		$sql='select * from productos';
			
		$result = $this->_db->paginar($sql,$pagina);
		return $result;
	}

	public function getProductoPorID($idproducto)
	{
		$sql="select * from productos where IDPRODUCTO ='$idproducto'";
			
		$result = $this->_db->query($sql);

		if ($result->num_rows) $datos = $result->fetch_object();
		else
		$datos= false;
		return $datos;
	}

	public function setProducto($idproducto,$descripcion, $precio, $video, $vigencia,$fabricante,$idioma,$edad, $mime, $foto ){

		$cadena = ($foto=='')?"":", MIME ='$mime', FOTO ='$foto'";
		
		$sql= "select idproducto from productos where IDPRODUCTO='$idproducto'";
		$result = $this->_db->query($sql);
		if ($result->num_rows) {
			$sql = "update productos set
					DESCRIPCION = '$descripcion',
					PRECIO = $precio,
					VIDEO = '$video',
					VIGENCIA = '$vigencia',
					FABRICANTE = '$fabricante',
					IDIOMA = '$idioma',
					EDAD = '$edad'
					$cadena
				where 
					IDPRODUCTO = '$idproducto'";
		}
		else {
			$sql = "insert into productos set
					DESCRIPCION = '$descripcion',
					PRECIO = $precio,
					VIDEO = '$video',
					VIGENCIA = '$vigencia',
					FABRICANTE = '$fabricante',
					IDIOMA = '$idioma',
					EDAD = '$edad',
					MIME = '$mime',
					FOTO = '$foto'
					";
			}
		$this->_db->query($sql);

	}




	public function getPaginacion()
	{
		if($this->_db->_paginacion){
			return $this->_db->_paginacion;
		} else {
			return false;
		}
	}
}

?>
