<?php
require_once 'config.php';
require_once __DIR__. '/php-activerecord/ActiveRecord.php';

class Libro extends ActiveRecord\Model { }

$cfg = ActiveRecord\Config::instance();
$cfg->set_model_directory('.');
$cfg->set_connections(array('development' => DB_ENGINE.'://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME));

foreach (Libro::find_all_by_autor_id(1) as $libro)
{
	echo $libro->nombre .' '.$libro->precio.' '.$libro->moneda.'<br>' ;
}
?>