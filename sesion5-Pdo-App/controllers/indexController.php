<?php

class indexController extends Controller
{
	public function __construct() {
		parent::__construct();

	}

	public function index()
	{
		$this->_view->titulo = 'administracion';
			
		$this->_producto= $this->loadModel('producto');
		$this->_view->productos = $this->_producto->getProductos();
		$this->_view->renderizar('index');
	}

	public function verFoto($idproducto){
		$this->_producto= $this->loadModel('producto');
		$this->_view->producto = $this->_producto->getProductoPorID($idproducto);
		header("Content-type: ".$this->_view->producto['MIME']);
		echo $this->_view->producto['FOTO'];
	}

	public function editar($idproducto)
	{
		$this->_view->titulo = 'Edicion de Reistros';
		$this->_view->setJs(array('editar'));
		$this->_producto= $this->loadModel('producto');
		$this->_view->producto = $this->_producto->getProductoPorID($idproducto);
		$this->_view->renderizar('editar');
	}

	public function grabarRegistro(){

		/* inicializar variables */
		$this->_producto= $this->loadModel('producto');
			
		$idproducto = isset($_POST['IDPRODUCTO'])?$_POST['IDPRODUCTO']:'';
		$descripcion = trim($_POST['DESCRIPCION']);
		$precio = trim($_POST['PRECIO']);
		$video =  trim($_POST['VIDEO']);
		$vigencia = trim($_POST['VIGENCIA']);
		$fabricante = trim($_POST['FABRICANTE']);
		$idioma = trim($_POST['IDIOMA']);
		$edad = trim($_POST['EDAD']);
		$mime ='';
		$foto = '';
			


		$this->_view->msg='';
		if (isset($_FILES['foto'])){
			$nombrearchivo= basename($_FILES['foto']['name']);
			$destino_archivo = "temp/".$nombrearchivo ;

			if(move_uploaded_file($_FILES['foto']['tmp_name'],$destino_archivo))
			{
				/* array de tipos de permitidos */
				$mimes_permitidos = array('image/jpeg','image/jpg','image/png');
				$mime = $_FILES['foto']['type'];

				if (in_array($mime, $mimes_permitidos))
				{
					$fp = fopen($destino_archivo,"rb");
					$contenido = fread($fp, filesize($destino_archivo));
					$foto = addslashes($contenido);
					fclose($fp);


				}
				else{
					$this->_view->msg = "Archivo no tiene el formato adecuado<br>";
				}
				unlink($destino_archivo);
			}

		}
		else
		$this->_view->msg = "No subio ningun Archivo.<br>";

		/* codigo de grabación */
		$this->_view->producto = $this->_producto->setProducto($idproducto,$descripcion,$precio,
		$video,$vigencia,$fabricante,$idioma,$edad,$mime,$foto);

		$this->_view->renderizar('grabar');
	}


}

?>