<?php

ini_set("soap.wsdl_cache_enabled","0");
$server = new SoapServer("hello.wsdl");

function saludo($nombre){
  return "Hola, ".$nombre;
}

$server->AddFunction("saludo");
$server->handle();
?>