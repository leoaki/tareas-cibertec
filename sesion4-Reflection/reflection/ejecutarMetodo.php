<?php
require_once("../clases/".$_GET['clase'].".php");
$params=array();

foreach($_GET as $name=>$param){
	if ($name!="clase" && $name!="metodo" && $name!="submit")	$params[]=$param;
}
//call_user_func_array es vital para crear el patron MVC, porque puedo especificar una clase,
//un metodo y parametros y debe ejecutarlo
$res=call_user_func_array(array($_GET['clase'], $_GET['metodo']), $params);
echo 'el resultado es => '.$res;
?>

