<?php


abstract class Composite {
  public abstract function doit();
}


class PlusOperator extends Composite {
  private $composites;

  public function __construct() {
    $this->composites = array();
  }

  public function add(Composite $composite) {
    array_push($this->composites, $composite);#array_push agrega un elemento al array
  }

  public function doit() {
    $sum = 0;

    foreach ($this->composites as $num)
      $sum += $num->doit();

    return $sum;
  }
}


class Number extends Composite {
  private $val;

  public function __construct($num) {
    $this->val = $num;
  }

  public function doit() {
    return $this->val;
  }
}


 $plus = new PlusOperator();
 $minus = new PlusOperator();

 
 
 $plus->add(new Number(1));
 $plus->add(new Number(2));
 
 $minus->add(new Number(-1));
 $minus->add(new Number(-2));
 

  echo  $plus->doit().'<br>';
  echo  $minus->doit().'<br>';
  $plus->add($minus);
  echo  $plus->doit().'<br>';
  

?>