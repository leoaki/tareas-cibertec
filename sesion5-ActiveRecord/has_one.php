<?php
require_once 'config.php';
require_once __DIR__. '/php-activerecord/ActiveRecord.php';

#has_one tiene un--> Libros tiene un contenido--relacion 1 a 1
class Libro extends ActiveRecord\Model { 
	static $belongs_to = array(array('autors'));
    static $has_one = array(array('contenidos'));
}

class Autor extends ActiveRecord\Model { 
	static $has_many = array(array('libros'));
	
}

class Contenido extends ActiveRecord\Model{
	static $belongs_to = array(array('libros'));
}

$cfg = ActiveRecord\Config::instance();
$cfg->set_model_directory('.');
$cfg->set_connections(array('development' => DB_ENGINE.'://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME));


$libro = Libro::find(1);

echo $libro->nombre. '<br>';
echo $libro->autors->nombre.'<br>';
echo $libro->contenidos->mime;
?>