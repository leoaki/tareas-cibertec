<?php
include_once 'config.php';
try {
	$dsn= DB_ENGINE.":host=".DB_HOST.";dbname=".DB_NAME;
	$dbh = new PDO($dsn,DB_USER,DB_PASS);
	$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
		$dbh=null;
		echo "Error de conexion con  la bd ".$e->getCode();
}

$idproducto=2;
$sql = 'SELECT IDPRODUCTO, DESCRIPCION, PRECIO FROM productos WHERE IDPRODUCTO = ? ';

$sth = $dbh->prepare($sql);
$sth->bindParam(1,$idproducto,PDO::PARAM_INT);
$sth->execute();

$producto= $sth->fetch();

print_r($producto).'<br>';


?>
