<?php include_once 'encabezado.php' ?>				
		<div id="style-switcher">
			<i class="icon-arrow-left icon-white"></i>
			<span>Style:</span>
			<a href="#grey" style="background-color: #555555;border-color: #aaaaaa;"></a>
			<a href="#blue" style="background-color: #2D2F57;"></a>
			<a href="#red" style="background-color: #673232;"></a>
		</div>
		
		<div id="content">
			<div id="content-header">
				<h1>Support chat</h1>
				<div class="btn-group">
					<a class="btn btn-large tip-bottom" title="Manage Files"><i class="icon-file"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Users"><i class="icon-user"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a>
					<a class="btn btn-large tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a>
				</div>
			</div>
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#">Sample pages</a>
				<a href="#" class="current">Suport chat</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="alert alert-success">This is an example of support chat. Feel free to write something.<a href="#" class="close" data-dismiss="alert">×</a></div>
						<div class="widget-box widget-chat">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-comment"></i>
								</span>
								<h5>Support chat</h5>
							</div>
							<div class="widget-content nopadding">
								<div class="chat-content panel-left">                   
								   <div class="chat-messages" id="chat-messages">
										<div id="chat-messages-inner"></div>
								   </div>									
								   <div class="chat-message well">
										<button class="btn btn-success">Send</button>  
										<span class="input-box">
											<input type="text" name="msg-box" id="msg-box" />
										</span>										                  
								   </div>                   
								</div>
								<div class="chat-users panel-right">
									<div class="panel-title"><h5>Online Users</h5></div>
									<div class="panel-content nopadding">
										<ul class="contact-list">
											<li id="user-michelle" class="online new"><a href="#"><img alt="" src="img/demo/av1.jpg" /> <span>Michelle</span></a><span class="msg-count badge badge-info">3</span></li>
											<li id="user-neytiri"><a href="#"><img alt="" src="img/demo/av2.jpg" /> <span>Neytiri</span></a></li>
											<li id="user-cartoon-man" class="online"><a href="#"><img alt="" src="img/demo/av3.jpg" /> <span>Cartoon Man</span></a></li>
											<li id="user-rick-newton" class="online"><a href="#"><img alt="" src="img/demo/av1.jpg" /> <span>Rick Newton</span></a></li>
											<li id="user-zack-renson" class="online new"><a href="#"><img alt="" src="img/demo/av2.jpg" /> <span>Zack Renson</span></a><span class="msg-count badge badge-info">1</span></li>
											<li id="user-vladimir-ivanov"><a href="#"><img alt="" src="img/demo/av3.jpg" /> <span>Wladimir Ivanov</span></a></li>
											<li id="user-master-moda"><a href="#"><img alt="" src="img/demo/av1.jpg" /> <span>Master Moda</span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row-fluid">
					<div id="footer" class="span12">
						2012 &copy; Unicorn Admin. Brought to you by <a href="https://wrapbootstrap.com/user/diablo9983">diablo9983</a>
					</div>
				</div>
			</div>
		</div>

            <script src="js/jquery.min.js"></script>
            <script src="js/jquery.ui.custom.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/unicorn.js"></script>
            <script src="js/unicorn.chat.js"></script>
	</body>

<!-- Mirrored from wbpreview.com/previews/WB0F35928/chat.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 06 Mar 2013 03:48:18 GMT -->
</html>
