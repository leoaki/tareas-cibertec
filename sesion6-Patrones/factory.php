<?php
 
class MySQL{
  public function __construct(){
    echo 'Instancio MySQL <br>';
  }
}
 
class PostgreSQL{
  public function __construct(){
    echo 'Instancio PostgreSQL <br>';
  }
}
 
abstract class BaseDeDatos{
    public static function cargar($tipo){
        try {
            if (class_exists($tipo)) {
                return new $tipo;
            } else {
                throw new Exception("No existe la clase '$tipo'");
            }
        } catch (Exception $e) {
            echo 'Excepci�n capturada: ',  $e->getMessage(), "\n";
        }
    }
}
 
BaseDeDatos::cargar("MySQL");

BaseDeDatos::cargar("PostgreSQL");

BaseDeDatos::cargar("Oracle");

BaseDeDatos::cargar("Leoaki");
?>