<?php 

//  ejemplo de reescritura del metodo y final
// define la clase
class Persona {
	
	// define las propiedades
	protected $dni;
	protected $nombre ;
	protected $edad;
	protected $talla;
	
	public function __construct($dni,$nombre,$edad,$talla){
		$this->dni = (int)$dni;
		$this->nombre = (string)$nombre;
		$this->edad = (int)$edad;
		$this->talla = (float)$talla;
		return;
	}
	
	public function __destruct(){
		echo 'Es el fin';
		
	}
	
} // fin de la clase



class Empleado extends Persona {
	private $id_essalud;
	
	public function __construct($dni,$nombre,$edad,$talla,$id_essalud){
		parent::__construct($dni,$nombre,$edad,$talla);
		$this->id_essalud=(string)$id_essalud;	
		return;
	}	
	
	public function getValues($propiedad){
		return $this->$propiedad;
		
	}
	
}

$vendedor = new Empleado('10254786','Juan Perez',32,1.74,'7011061SCMEF005');
#$vendedor->dni='leoaki';// no funciona porque el atributo es protected
?>


<html>
<head>

</head>
<body>
	<table border=1>
		<tr>
			<th>Item</th>
			<th>Valor</th>
		</tr>
		<tr>
			<td>DNI</td>
			<td><?=$vendedor->getValues('dni')?></td>
		</tr>	
		<tr>
			<td>NOMBRE</td>
			<td><?=$vendedor->getValues('nombre') ?></td>
		</tr>
		<tr>
			<td>EDAD</td>
			<td><?=$vendedor->getValues('edad') ?></td>
		</tr>
		<tr>
			<td>TALLA</td>
			<td><?=$vendedor->getValues('talla')?></td>
		</tr>
		<tr>
			<td>ESSALUD</td>
			<td><?=$vendedor->getValues('id_essalud')?></td>
		</tr>
	</table>
</body>
</html>