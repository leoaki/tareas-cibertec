<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= isset($this->titulo)?$this->titulo:''; ?></title>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>tienda.css" >
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/jquery.min.js"></script>
	
		
	
    <!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<? if(isset($_layoutParams['js']) && count($_layoutParams['js'])): ?>
		<? foreach($_layoutParams['js'] as $layout): ?>
   			<script src="<?= $layout ?>" type="text/javascript"></script>
		<? endforeach; ?>
	<? endif; ?>
	
</head>

<body>
 <div class="topbar">
     <div class="fill">
        <div class="container">
          <a class="brand" href="<?= BASE_URL?>index/index">Mi tienda</a>
       		<div class="row">
       			<div  class="span4" style="background:#fff;float:right;">
       			  <a href="<?= BASE_URL?>index/carro"><img src="<?= $_layoutParams['ruta_img']?>carrito.png" alt="carro">
					<? if(isset($_SESSION["totalImporte"])):?> 
                 		total:<?php echo $_SESSION["cantidadTotal"];?>
						$ <?php echo $_SESSION["totalImporte"];?> 
                    <? else:?>
                        total: 0
                		$     0
                	<? endif;?>
             	   </a>
				</div>
       		</div>
       	</div>
     </div>
</div>
