<?php

class entidadModel extends Model
{
	public function __construct() {
		parent::__construct();
	}

	public function getDepartamentos(){
		$sql="SELECT DEPARTAMENTO,NOMBRE FROM cat_entidad  WHERE
		DEPARTAMENTO<>'0' 
		AND PROVINCIA='0'";
		$result = $this->_db->query($sql);
		return $result;
	}

	public function getProvincias($departamento){
		$sql="SELECT PROVINCIA,NOMBRE FROM cat_entidad  WHERE
		DEPARTAMENTO='$departamento' 
		AND PROVINCIA<>'0' 
		AND DISTRITO='0';";
		$result = $this->_db->query($sql);
		return $result;
	}

	public function getDistritos($departamento,$provincia){
		$sql="SELECT DISTRITO,NOMBRE FROM cat_entidad  WHERE
		DEPARTAMENTO='$departamento' 
		AND PROVINCIA='$provincia' 
		AND DISTRITO!='0'";
		$result = $this->_db->query($sql);
		return $result;
	}
	
	
	public function getCalles($calle){
		$sql ="SELECT 
				CONCAT(CALLE,' ',CUADRA) as DIRECCION,LATITUD,LONGITUD 
			   FROM 
			   	cat_calle 
			   WHERE 
			   	CALLE LIKE '%$calle%'
			   GROUP BY DIRECCION LIMIT 0,10	
			   	";
		$result = $this->_db->query($sql);
		return $result;
	}
}
#Replicacion en base de datos
?>
