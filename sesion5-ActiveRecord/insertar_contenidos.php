<?php
require_once 'config.php';
require_once __DIR__. '/php-activerecord/ActiveRecord.php';

class Contenido extends ActiveRecord\Model { }

$cfg = ActiveRecord\Config::instance();
$cfg->set_model_directory('.');
//$cfg->set_connections(array('development' => 'mysql://userlibreria:12345@localhost/libreria'));
$cfg->set_connections(array('development' => DB_ENGINE.'://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME));


$contenido1 = new Contenido(array('libro_id' => 1, 'mime' => 'application/pdf.pdf', 'data' => 'Los jefes contenido'));
$contenido1->save();

$contenido2 = Contenido::create(array('libro_id' => 2, 'mime' => 'application/pdf2.pdf', 'data' => 'El loquillo Mart�n'));

echo 'registros grabados';
?>