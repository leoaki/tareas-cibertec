<?php

/*
 * -------------------------------------
 *  Config.php
 * -------------------------------------
 */
define('BASE_URL', 'https://192.168.0.111/');
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_LAYOUT', 'default');

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'soporte');
define('DB_NAME', 'tienda_virtual');
define('LIMIT_REGXPAG',5);
define('LIMIT_PAGEVIEW',10);
define('DB_ENGINE','mysql');
define('DB_CHAR', 'utf8');

?>
