<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= isset($this->titulo)?$this->titulo:''; ?></title>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>bootstrap.css" />	
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>real_estate.css" />
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>jquery.ui.css" />
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>jquery.aw-showcase/style.css" />
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>badger/badger.min.css" />
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>sticky/sticky.min.css" />	
		
		
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/jquery.ui.custom.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/jquery.aw-showcase/jquery.aw-showcase.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/badger/badger.min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/sticky/sticky.min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/portamento-min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/global.js"></script>	
	<script type="text/javascript" src="<?= BASE_URL; ?>views/layout/default/js/login.js"></script>	
	
    <!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<? if(isset($_layoutParams['js']) && count($_layoutParams['js'])): ?>
		<? foreach($_layoutParams['js'] as $layout): ?>
   			<script src="<?= $layout ?>" type="text/javascript"></script>
		<? endforeach; ?>
	<? endif; ?>
	
</head>

<body>
 <div class="container">
		<div class="row"><!-- start header -->
			<div class="span4 logo">
				<a href="index.html">
				<div class="row">
					<div class="span1">
						<img src="<?= $_layoutParams['ruta_img']?>Home-green-48.png" alt=""/>
					</div>
					<div class="span3">
						<h1><small>Administraci�n</small><br />Mi Tienda</h1>
					</div>
				</div> 
				</a>
			</div>		
			<div class="span4 customer_service pull-right text-right">
				<br />
				<h4>Telf: 42372406</h4>
				<h4><small>24 horas al d�a, 7 dias a la semana</small></h4>
			</div>
		</div><!-- end header -->
		
		<div class="row"><!-- start nav -->
			<div class="span12">
				<div class="navbar">
					<div class="navbar-inner">
						<div class="container">
							<div class="nav-collapse">
							   <ul class="nav">
									<li><a href="<?= BASE_URL; ?>index/index" class="first">Productos</a></li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">PDFs <b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a href="<?= BASE_URL; ?>pdf/index">PDF1</a></li>
											<li><a href="<?= BASE_URL; ?>pdf/index2">PDF2</a></li>
											<li><a href="<?= BASE_URL; ?>pdf/index3">PDF3</a></li>
											
										</ul>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">AJAXs<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a href="<?= BASE_URL; ?>ajax/index">AJAX1</a></li>
											<li><a href="<?= BASE_URL; ?>ajax/sugeridos">AJAX2</a></li>
											<li><a href="<?= BASE_URL; ?>ajax/index3">AJAX3</a></li>
											
										</ul>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Graficos<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a href="<?= BASE_URL; ?>graph/imagen1">Imagen1</a></li>
											<li><a href="<?= BASE_URL; ?>graph/imagen2">Imagen2</a></li>
											<li><a href="<?= BASE_URL; ?>graph/imagen3">Imagen3</a></li>
											<li><a href="<?= BASE_URL; ?>graph/imagen4">Imagen4</a></li>
											<li><a href="<?= BASE_URL; ?>graph/imagen5">Imagen5</a></li>
											<li><a href="<?= BASE_URL; ?>graph/imagen6/22">Imagen6</a></li>
											<li><a href="<?= BASE_URL; ?>graph/grafico1">Grafico1</a></li>
											<li><a href="<?= BASE_URL; ?>graph/grafico2">Grafico2</a></li>
											<li><a href="<?= BASE_URL; ?>graph/grafico3">Grafico3</a></li>
											<li><a href="<?= BASE_URL; ?>graph/grafico4">Grafico4</a></li>
											<li><a href="<?= BASE_URL; ?>graph/grafico5">Grafico5</a></li>
											<li><a href="<?= BASE_URL; ?>graph/carrusel">Carrusel</a></li>
										</ul>
									</li>
									</ul>
									<ul class="nav pull-right">
									
									<li class="dropdown">
										<a class="dropdown-toggle" href="#" data-toggle="dropdown">Registrate<strong class="caret"></strong></a>
										<div class="dropdown-menu">
											<form action="<?= BASE_URL; ?>usuario/nuevo" method="post">
												<div id='formulario'>
												<input type="text" name="nombre" placeholder="Nombre completo" size="30" />
												<input type="text" name="email" placeholder="Email" size="30" />
												<input type="password" name="contrasenia" placeholder="Contrase�a" size="30" />
												
												<input class="btn btn-primary" type="button" id='btnregistrar' value="Registrar" />
												</div>
											</form>
										</div>
									</li>			
									
									<li class="dropdown">
										<a class="dropdown-toggle" href="#" data-toggle="dropdown">Iniciar sesi�n <strong class="caret"></strong></a>
										<div class="dropdown-menu">
											<form action="index.html" method="post">
												<input type="text" name="user[username]" placeholder="Email" size="30" />
												<input type="password" name="user[password]" placeholder="Contrase�a" size="30" />
												<input id="remember_me" type="checkbox" name="user[remember_me]" value="1" />
												<label class="string optional"> Recordar su contrase�a</label>
												
												<input class="btn btn-primary" type="submit" name="commit" value="Ingresar" />
											</form>
										</div>
									</li>
									
								</ul>
									
									
									
												
						
							</div><!-- /.nav-collapse -->
						</div>
					</div><!-- /navbar-inner -->
				</div><!-- /navbar -->
			</div>
		</div><!-- end nav -->		
		
