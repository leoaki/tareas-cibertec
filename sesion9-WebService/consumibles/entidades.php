<?php
$client = new SoapClient("http://www.mobilefish.com/services/web_service/countries.php?wsdl",array('trace' => 1));
$pais = 'ec';
$result = $client->regionsInfoByIana($pais);
?>
<!DOCTYPE table PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<table border=1>
	<tr>
		<th>PAIS</th>
		<th>REGION</th>
		<th>LATITUD</th>
		<th>LONGITUD</th>
	</tr>
	<? foreach($result as $registro):?>
		<tr>
			<td><?=$registro->ianacode?></td>
			<td><?=$registro->regionname?></td>
			<td><?=$registro->latitude?></td>
			<td><?=$registro->longitude?></td>
		</tr>
	<?endforeach;?>
</body>
</html>



