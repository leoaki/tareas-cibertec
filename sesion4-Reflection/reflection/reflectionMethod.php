<?php

header('Content-Type: text/plain');

class ejemplo
{

    /**
     * Este es mi DocComment!
     *
     * @autor:  Frank Sanchez 
     * @DocTag: imprime Hola mundo!
     */
    public function metodoA()
    {
        echo 'Hola Mundo!';
    }
    
    /**
     * Este es mi DocComment!
     *
     * @autor:  Frank Sanchez 
     * @DocTag: imprime Adios amigos!
     */
   public function metodoB()
   {
   		echo 'Adios Amigos';
   }
    
    
}

function getDocComment($str, $tag = '')
{
    if (empty($tag))
    {
        return $str;
    }

    $matches = array();
    preg_match("/".$tag.":(.*)(\\r\\n|\\r|\\n)/U", $str, $matches);

    if (isset($matches[1]))
    {
        return trim($matches[1]);
    }

    return '';
}

$method = new ReflectionMethod('ejemplo', 'metodoB');

echo getDocComment($method, '@autor');

?> 