<?php
class classOperaciones {

	function suma($a,$b){
		$resultado =$a+$b;
		return $resultado;
	}

	function resta($a,$b){
		$resultado =$a-$b;
		return $resultado;
	}
	
	function producto($a,$b){
		$resultado = $a*$b;
		return $resultado;
	}

	function division($a,$b){
		try {
			if (!$b) throw new Exception('Division por cero');
			$resultado = $a/$b;
			return $resultado;
		}
		catch (Exception $e){
			return 'Error '.$e->getMessage();
			
		}
		
	}
}

?>