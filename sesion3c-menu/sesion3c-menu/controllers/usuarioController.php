<?php

class usuarioController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}

	public function index()
	{  // genera la grilla de registros de la tabla usuario


		$this->_usuario = $this->loadModel('usuario');
		$this->_view->usuarios = $this->_usuario->getAllUsuario();
		$this->_view->renderizar('index');

	}

	public function addUsuario(){
		// recepcion de datos del formulario  y limpieza de los espacios en blanco
		$usuario = trim($_POST['usuario']);
		$nombre = trim($_POST['nombre']);
		$contrasenia = trim($_POST['contrasenia']);
		$validate = trim($_POST['rpcontrasenia']);
		
		// carga el modelo y llama al metodo para insertar un usuario
		if ($usuario!=''){
		if ($contrasenia == $validate){		
		$this->_usuario = $this->loadModel('usuario');
		$result = $this->_usuario->setUsuario($usuario,$nombre,$contrasenia);

		// validacion del error al grabar
		if ($result)  $this->_view->error ='Error al registrar usuario';
		}
		else $this->_view->error='Las contrasenas no son iguales';
		}
		else $this->_view->error='No ingreso el usuario';
		$this->_view->renderizar('index');
	}

	public function desactivar($idusuario){

		$this->_usuario = $this->loadModel('usuario');
		$result = $this->_usuario->offUsuario($idusuario);

		if ($result)  $this->_view->error ='Error al desactivar el usuario';
		$this->_view->renderizar('index');
	}


}