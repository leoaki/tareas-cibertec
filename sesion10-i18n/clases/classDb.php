<?

class Db extends mysqli
{
	
	function __construct()
	{
	
		parent::connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		parent::query("SET time_zone =".ZONA_TIME);
		date_default_timezone_set(ZONA_TIME);
			
		return;
	}

	function insert($table_name,$field){

		foreach ($field as $index => $value)
		{
			if (is_string($value))  $value=utf8_decode(trim($value));
			$value = addslashes($value);

			if (!isset($sql)) $sql =" $index = '$value'";
			else
			$sql.= ", $index = '$value'";

		}
		$sql = "INSERT INTO $table_name SET " . $sql;

		return  $this->query($sql);

	}

	function cmbselect_db($field_name,$sql,$selected,$attr,$addoption='TODOS')
	{

		echo '<select name='."'$field_name'".' '.$attr. ' >';

		if ($addoption != null) echo "<option value='' selected>$addoption</option>";

		$result= $this->query($sql);
		while ($reg=$result->fetch_array(MYSQLI_NUM)){
			if ($selected==$reg[1] or $selected==$reg[0] )
			echo '<option value='."'$reg[0]'".' selected >'.utf8_encode($reg[1]).'</option>';
			else
			echo '<option value='."'$reg[0]'".' >'.utf8_encode($reg[1]).'</option>';
		}
		echo '</select>';
	}

}
?>