<?php

class indexController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}

	public function index()
	{
		$this->_view->titulo = 'Portada';


		$this->_view->renderizar('index');
	}

	 public function login()
        {
                $this->_view->titulo = 'login';
                $this->_model = $this->loadModel('usuario');
                if (isset($_POST['usuario']) && isset($_POST['contrasenia'])){
                        $usuario = trim($_POST['usuario']);
                        $contrasenia = trim($_POST['contrasenia']);
                        $user = $this->_model->existeUsuario($usuario, $contrasenia);
                        if ($user){
                                $_SESSION['usuario_app01'] = $user;
                        }else{
                                $this->_view->error = 'Usuario o contrase&ntilde;a incorrectos';
                        }
                }
                $this->_view->renderizar('index');
        }
	
	
	public function logout(){
		unset($_SESSION['usuario_app01']);		
		$this->_view->renderizar('index');
	}
}

?>