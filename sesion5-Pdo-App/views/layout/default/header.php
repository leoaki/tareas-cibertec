<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= isset($this->titulo)?$this->titulo:''; ?></title>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>bootstrap.css" />	
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>real_estate.css" />
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>jquery.aw-showcase/style.css" />
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>badger/badger.min.css" />
	<link rel="stylesheet" href="<?= $_layoutParams['ruta_css'];?>sticky/sticky.min.css" />	
		
		
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/jquery.aw-showcase/jquery.aw-showcase.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/badger/badger.min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/sticky/sticky.min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/portamento-min.js"></script>
	<script type="text/javascript" src="<?= BASE_URL; ?>public/js/global.js"></script>	
		
	
    <!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<? if(isset($_layoutParams['js']) && count($_layoutParams['js'])): ?>
		<? foreach($_layoutParams['js'] as $layout): ?>
   			<script src="<?= $layout ?>" type="text/javascript"></script>
		<? endforeach; ?>
	<? endif; ?>
	
</head>

<body>
 <div class="container">
		<div class="row"><!-- start header -->
			<div class="span4 logo">
				<a href="index.html">
				<div class="row">
					<div class="span1">
						<img src="<?= $_layoutParams['ruta_img']?>Home-green-48.png" alt=""/>
					</div>
					<div class="span3">
						<h1><small>Administraci�n</small><br />Mi Tienda</h1>
					</div>
				</div> 
				</a>
			</div>		
			<div class="span4 customer_service pull-right text-right">
				<br />
				<h4>Telf: 42372406</h4>
				<h4><small>24 horas al d�a, 7 dias a la semana</small></h4>
			</div>
		</div><!-- end header -->
		
		<div class="row"><!-- start nav -->
			<div class="span12">
				<div class="navbar">
					<div class="navbar-inner">
						<div class="container">
							<div class="nav-collapse">
							   <ul class="nav">
									<li><a href="<?= BASE_URL; ?>index/index" class="first">Productos</a></li>
									
								</ul>
								
							</div><!-- /.nav-collapse -->
						</div>
					</div><!-- /navbar-inner -->
				</div><!-- /navbar -->
			</div>
		</div><!-- end nav -->		
		
