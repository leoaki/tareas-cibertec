<?
class usuarioModel extends Model
{
	public function __construct() {
		parent::__construct(); //este constructor inicia la conexion a la base de datos.
	}

	public function getUsuarios($pagina =1)
	{
		$sql = 'select * from usuario';
		$result = $this->_db->paginar($sql, $pagina);
		return $result;
	}
	
	public function getPaginacion()
	{
		if (isset($this->_db->_paginacion))
		{
			return $this->_db->_paginacion;	
		}
		else 
		{
			return false;	
		}
	}
	
	public function existeUsuario($usuario, $contrasenia)
	{
		$sql="select IDUSUARIO from usuario where IDUSUARIO ='$usuario' and CONTRASENIA= sha1('$contrasenia')";
		$result = $this->_db->query($sql);
		return $result->num_rows;
	}
	
	public function getAllUsuario()
	{
		$sql="select IDUSUARIO,NOMBRE,ROL,FECHACREACION,FECHAMODIFICACION,ACTIVO from usuario ";
		$result = $this->_db->query($sql);
		return $result;
	}
	
	public function offUsuario($usuario)
	{
		$sql="update usuario set ACTIVO = '0' where IDUSUARIO='$usuario'";
		$result = $this->_db->query($sql);
		return $this->_db->errno;
	}
}

?>
