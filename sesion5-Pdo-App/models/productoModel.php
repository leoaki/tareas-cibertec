<?php

class productoModel extends Model
{
	public function __construct() {
		parent::__construct();
	}

	public function getProductos()
	{
		$result = $this->_db->query("select * from productos");
		return $result->fetchall();
		
	}

	public function getProductoPorID($idproducto)
	{
		$idproducto = (int) $idproducto;
        $prod = $this->_db->prepare("select * from productos where IDPRODUCTO =:idproducto ");
		$prod->execute(array(':idproducto'=>$idproducto));
        return $prod->fetch();
	
	}

	public function setProducto($idproducto,$descripcion, $precio, $video, $vigencia,$fabricante,$idioma,$edad, $mime, $foto ){

		//$cadena = ($foto=='')?"":", MIME ='$mime', FOTO ='$foto'";

		
		$prod =  $this->_db->prepare("select * from  productos where IDPRODUCTO =:idproducto" );
		$prod->execute(array(':idproducto'=>$idproducto));
		
		if ($prod->rowCount()){
		$result = $this->_db->prepare("UPDATE productos SET 
		 					DESCRIPCION = :descripcion,
		 					PRECIO = :precio,
		 					VIDEO = :video,
		 					VIGENCIA = :vigencia,
		 					FABRICANTE = :fabricante,
		 					IDIOMA = :idioma,
		 					EDAD = :edad
		 					WHERE IDPRODUCTO = :idproducto");
           $result->execute(array(
                           ':idproducto' => $idproducto ,
                           ':descripcion' => $descripcion ,
                           ':precio' => $precio,
           				   ':video' => $video,
           				   ':vigencia' => $vigencia,
           				   ':fabricante' => $fabricante,
           				   ':idioma' => $idioma,
           				   ':edad' => $edad
                           ));
		
		}
		else {
		 $this->_db->prepare("INSERT INTO productos (IDPRODUCTO,DESCRIPCION,PRECIO,VIDEO,VIGENCIA,FABRICANTE,IDIOMA,EDAD) 
		 				VALUES (null, :descripcion, :precio, :video, :vigencia, :fabricante, :idioma, :edad ")
                ->execute(
                        array(
                           ':descripcion' => $descripcion,
                           ':precio' => $precio,
                           ':video' => $video,
                           ':vigencia' => $vigencia,
                           ':fabricante' => $fabricante,
                           ':idioma'=> $idioma,
                           ':edad' => $edad
                        ));
		
		}
	

	}




	public function getPaginacion()
	{
		if($this->_db->_paginacion){
			return $this->_db->_paginacion;
		} else {
			return false;
		}
	}
}

?>
