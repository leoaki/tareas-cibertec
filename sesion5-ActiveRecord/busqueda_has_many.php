<?php
/*  relacion has_many */

require_once 'config.php';
require_once __DIR__. '/php-activerecord/ActiveRecord.php';

//$belong_to pertenece a---> un libro le pertenece a 1 autor
class Libro extends ActiveRecord\Model { 
	static $belongs_to = array(array('autors'));
}

//$has_many puede tener---> un autor puede tener varios libros
class Autor extends ActiveRecord\Model { 
	static $has_many = array(array('libros'));
	
}



$cfg = ActiveRecord\Config::instance();
$cfg->set_model_directory('.');
$cfg->set_connections(array('development' => DB_ENGINE.'://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME));

$libro = Libro::find(2);

echo 'Nombre : '. $libro->nombre. '<br>';
echo 'Precio : '. $libro->precio. '<br>';
echo 'Autor : '. $libro->autors->nombre.'<br>';

?>