<?php

class productoController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}

	public function index($pagina=1)
	{
		$this->_view->titulo = 'administracion';
			
		$this->_producto= $this->loadModel('producto');
	
		$this->_view->productos = $this->_producto->getProductos($pagina);
			
		$this->_view->param = $this->_producto->getPaginacion();
	
	
		$this->_view->renderizar('index');
	}

	public function verFoto($idproducto){
		$this->_producto= $this->loadModel('producto');
		$this->_view->producto = $this->_producto->getProductoPorID($idproducto);
		if(is_object($this->_view->producto));{
			header("Content-type: ".$this->_view->producto->MIME);
			echo $this->_view->producto->FOTO;
		}
	}

	public function editar($idproducto)
	{
		$this->_view->titulo = 'Edicion de Registros';

		//no es necesario que tengan el mismo nomnbre.
		$this->_view->setJs(array('editar'));
		
		$this->_producto= $this->loadModel('producto');
		$this->_view->producto = $this->_producto->getProductoPorID($idproducto);
		
		// siempre se renderizar un html
		$this->_view->renderizar('editar');
	}


	public function grabarRegistro(){

		/* inicializar variables */
		$this->_producto= $this->loadModel('producto');
			
		$idproducto = isset($_POST['IDPRODUCTO'])?$_POST['IDPRODUCTO']:'';
		$descripcion = trim($_POST['DESCRIPCION']);
		$precio = trim($_POST['PRECIO']);
		$video =  trim($_POST['VIDEO']);
		$vigencia = trim($_POST['VIGENCIA']);
		$fabricante = trim($_POST['FABRICANTE']);
		$idioma = trim($_POST['IDIOMA']);
		$edad = trim($_POST['EDAD']);
		$mime ='';
		$foto = '';
			


		$this->_view->msg='';
		if (isset($_FILES['foto'])){
			//ubicar el nombre de archivo.
			$nombrearchivo= basename($_FILES['foto']['name']);
			//ubicar la carpeta donde se va alojar.
			$destino_archivo = "temp/".$nombrearchivo ;
			
			//mover el archivo.
			if(move_uploaded_file($_FILES['foto']['tmp_name'],$destino_archivo))
			{
				/* array de tipos de permitidos */
				$mimes_permitidos = array('image/jpeg','image/jpg','image/png');
				$mime = $_FILES['foto']['type'];

				//para buscar un dato dentro de un array
				if (in_array($mime, $mimes_permitidos))
				{
					//realizamos la apertura.
					//rb = modo lectura 
					$fp = fopen($destino_archivo,"rb");
					//calcular el tama�o del archivo.
					$contenido = fread($fp, filesize($destino_archivo));
					//se valida la cadena.
					$foto = addslashes($contenido);
					fclose($fp);


				}
				else{
					$this->_view->msg = "Archivo no tiene el formato adecuado<br>";
				}
				
				//se utiliza para borrar un archivo html
				unlink($destino_archivo);
			}

		}
		else
		$this->_view->msg = "No subio ningun Archivo.<br>";

		/* codigo de grabaci�n */
		$this->_view->producto = $this->_producto->setProducto($idproducto, $descripcion, $precio,
		$video, $vigencia, $fabricante, $idioma, $edad, $mime, $foto);

		$this->_view->redireccionar('producto/index');
	}


}

?>