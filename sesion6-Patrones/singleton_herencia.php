<?php 
include_once 'config.php';

class Conexion extends PDO
{
    private static $dbh;

	 public static function singlenton() {
    	if (!isset(self::$dbh)) {
                self::$dbh =  new PDO( DB_ENGINE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
        }
        return self::$dbh;
    }
    
    public function socket_conexion(){
    	
    	 return self::$dbh;
    }
    
}


class Alumno extends Conexion
{
	public function __construct(){
		return parent::singlenton();
		
	}
	
	
}

class Curso extends Conexion
{
	public function __construct(){
		return parent::singlenton();
		
	}
	
}

/* PROGRAMA PRINCIPAL */ 
$AlumnoPhp = new Alumno();
$CursoPhp = new Curso();

if ($AlumnoPhp->socket_conexion() === $CursoPhp->socket_conexion()) echo 'Manejan la misma conexion';
else echo 'Las conexiones son diferentes';
    
?>


