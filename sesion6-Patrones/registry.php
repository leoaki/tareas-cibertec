<?php 
class Registry {
	private static $register;
	
	public static function  add($item){
		if (is_object($item)){
			$name = get_class($item);
		}else {
			throw new Exception('Objeto incorrecto');
		}
		self::$register[$name]=$item;
	}
	
	public static function get($name){
		if (array_key_exists($name, self::$register)){
			return self::$register[$name];
		}else {
			throw  new Exception('Clase no registrada');
		}
	}
	
	
}

class Alumno {
	public $aula = 'A101';
}


class Curso {
	public $aula = 'A102';
	
}


$AlumnoPHP = new Alumno;
$cursoPHP = new Curso;
Registry::add($AlumnoPHP);
Registry::add($cursoPHP);

echo Registry::get('Curso')->aula;

?>