<?php


class Persona{
	
	private $nombre;
	private $edad;
	private $peso;
	
	public function __construct($nombre,$edad,$peso){
		$this->_setNombre($nombre);
		$this->_setEdad($edad);
		$this->_setPeso($peso);
	}
	
	private function _setNombre($nombre){
		$this->nombre= trim($nombre);
	}
	
	private function _setEdad($edad){
		$this->edad = (int)$edad;
	}
	
	private function _setPeso($peso){
		$this->peso = (float)$peso;
		
	}
	public function _getNombre(){
		return $this->nombre;
	}
	
	public function _getEdad(){
		return $this->edad;
	}
	
	public function _getPeso(){
		return (int)$this->peso;
	}
	
}



$usuario = new Persona('Luis Calderon','23s','85.5');

echo $usuario->_getNombre().'<br>';
echo $usuario->_getEdad().'<br>';
echo $usuario->_getPeso().'<br>';
?>