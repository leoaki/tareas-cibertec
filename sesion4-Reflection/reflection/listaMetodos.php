<?php

$metodos=array();
//file_exits verifica si existe un archivo
if (isset($_GET['clase']) && file_exists('../clases/'.$_GET['clase'].'.php')){
	require_once '../clases/'.$_GET['clase'].'.php';
		
	$class = new ReflectionClass($_GET['clase']);
	
	foreach($class->getMethods() as $m)
		$metodos[]=$m->name;
	
	$metodos=array_merge(array("Escoge Metodo"), $metodos);
}else{
	echo 'NO HAS ELEGIDO CLASE';
	header('Location: listaClases.php');
}
?>
<!DOCTYPE select PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
Seleccione el metodo de la clase <?= $_GET['clase'] ?><br><br>
<form action='listaParametros.php' method='get'>
<input type='hidden' name='clase' value="<?=$_GET['clase']?>">

<select name='metodo'>
<?php foreach ($metodos as $options):?>
  <option><?=$options?></option>
<?php endforeach;?>
</select>

<br><br><br><br>

<input type='submit' value='enviar'>

</form>
</body>
</html>