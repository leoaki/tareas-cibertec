<?php

class ajaxController extends Controller
{
	public function __construct() {
		parent::__construct();

	}

	public function  index()
	{
		$this->_view->titulo = 'Prueba de  Ajax';
		$this->_entidad= $this->loadModel('entidad');
		$this->_view->departamentos = $this->_entidad->getDepartamentos();

		$this->_view->setJs(array('index'));
		$this->_view->renderizar('index');
	}

	public function provincias($departamento){

		$this->_view->titulo = 'Prueba de  Ajax';
		$this->_entidad= $this->loadModel('entidad');
		$this->_view->provincias = $this->_entidad->getProvincias($departamento);


		$this->_view->renderizar('ajax_provincias',true);
	}

	public function distritos($departamento,$provincia){

		$this->_view->titulo = 'Prueba de  Ajax';
		$this->_entidad= $this->loadModel('entidad');
		$this->_view->distritos = $this->_entidad->getDistritos($departamento,$provincia);


		$this->_view->renderizar('ajax_distrito',true);
	}


	public function sugeridos()
	{
		$this->_view->titulo = 'Prueba de  Ajax';
		
		$this->_view->setJs(array('sugeridos'));
		$this->_view->renderizar('sugeridos');
	}

	public function buscaCalle(){

		$calle= $_GET['term'];
		$this->_entidad= $this->loadModel('entidad');
		$result = $this->_entidad->getCalles($calle);

		while($reg = $result->fetch_object())
		{
			$data[]=array('label'=>$reg->DIRECCION,'latitud'=>$reg->LATITUD,'longitud'=>$reg->LONGITUD);
		}
		echo json_encode($data);

	}
}

?>