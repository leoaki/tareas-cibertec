<?php

include_once 'Config.php';

class Usuario extends mysqli
{
	public function __construct() {
		parent::__construct(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if ($this->connect_errno)	die('Connect Error: ' . $this->connect_errno);
		return;
	}
	
	public function insertReg($idusuario,$nombre,$contrasenia,$activo,$rol){
		$sql="insert into usuarios set IDUSUARIO ='$idusuario', 
										NOMBRE ='$nombre',
										CONTRASENIA = SHA1('$contrasenia'), 
										ACTIVO='$activo', 
										ROL='ADMINISTRADOR', 
										FECHACREACION= NOW()";
		$this->query($sql);
		return $this->errno;
	}
	
	public function updateReg($idusuario,$nombre,$contrasenia,$activo,$rol){
		$sql="update usuarios set  NOMBRE ='$nombre', CONTRASENIA = SHA1('$contrasenia'), ACTIVO='$activo', ROL='ADMINISTRADOR' where IDUSUARIO ='$idusuario'";
		$this->query($sql);
		return $this->errno;
	}

	public function deleteReg($idusuario){
		$sql="delete from  usuarios where IDUSUARIO ='$idusuario'";
		$this->query($sql);
		return $this->errno;
	}
	
	public function getAllReg(){
		$sql="select * from  usuarios";
		return  $this->query($sql);
	}
	
	public function getIdReg($idusuario){
		$sql="select * from  usuarios where IDUSUARIO ='$idusuario'";
		return  $this->query($sql);
	}
}

// instancia del objeto
$con = new Usuario;
//echo 'Error producido al insertar '.$con->insertReg('Laki','Leo Aki','Yasmina0',1,'ADMINISTRADOR') .'<br>';
//echo 'Error producido al actualizar '. $con->updateReg('jperez','Juan Perez Rodriguez','12345',1,'ADMINISTRADOR').'<br>';
#echo 'Error producido al eliminar '. $con->deleteReg('jperez').'<br>';
	?>
