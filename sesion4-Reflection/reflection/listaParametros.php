<?php

require_once("../clases/".$_GET['clase'].".php");

$parametros=array();

if (isset($_GET['metodo'])){
	
	$class = new ReflectionClass($_GET['clase']);
	$param = $class->getMethod($_GET['metodo']);
	foreach ($param->getParameters() as $i => $parametro)
	$parametros[]=$parametro->getName();

    }
?>

<html>
<body>
Ingreso de parametros para la clase <?=$_GET['clase']?> y metodo <?= $_GET['metodo']?><br>
<form action='ejecutarMetodo.php' method='get'>
<input type='hidden' name='clase' value="<?=$_GET['clase']?>">
<input type='hidden' name='metodo' value="<?=$_GET['metodo']?>">
<?php foreach ($parametros as $campo):?>
<p>Parametro <?=$campo ?> <input type='text' name='<?=$campo?>' ></p>

<?php endforeach; ?>
<br><br>
<input type='submit' value='Enviar'>
</form>
</body>
</html>