<?php

require_once 'config.php';
require_once 'autoload.php';

function datos_persona($dni) {
	$con = new Persona();
	$persona =$con->getPersona($dni);
	return json_encode($persona);
}


$options = array('uri' => 'http://localhost/ws/server/JSONserver.php');
$soap = new SoapServer(null, $options);

$soap->addFunction("datos_persona");

$soap->handle();

?>