<?php include_once 'encabezado.php' ?>				
		<div id="style-switcher">
			<i class="icon-arrow-left icon-white"></i>
			<span>Style:</span>
			<a href="#grey" style="background-color: #555555;border-color: #aaaaaa;"></a>
			<a href="#blue" style="background-color: #2D2F57;"></a>
			<a href="#red" style="background-color: #673232;"></a>
		</div>
		
		<div id="content">
			<div id="content-header">
				<h1>Gallery</h1>
				<div class="btn-group">
					<a class="btn btn-large tip-bottom" title="Manage Files"><i class="icon-file"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Users"><i class="icon-user"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a>
					<a class="btn btn-large tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a>
				</div>
			</div>
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#">Sample pages</a>
				<a href="#" class="current">Gallery</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-picture"></i>
								</span>
								<h5>Gallery</h5>
							</div>
							<div class="widget-content">
								<ul class="thumbnails">
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="http://placehold.it/140x140" alt="">
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row-fluid">
					<div id="footer" class="span12">
						2012 &copy; Unicorn Admin. Brought to you by <a href="https://wrapbootstrap.com/user/diablo9983">diablo9983</a>
					</div>
				</div>
			</div>
		</div>
		
		
            
            <script src="js/jquery.min.js"></script>
            <script src="js/jquery.ui.custom.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/unicorn.js"></script>
	</body>

<!-- Mirrored from wbpreview.com/previews/WB0F35928/gallery.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 06 Mar 2013 03:48:18 GMT -->
</html>
