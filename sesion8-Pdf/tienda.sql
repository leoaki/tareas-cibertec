-- MySQL dump 10.11
--
-- Host: localhost    Database: tienda
-- ------------------------------------------------------
-- Server version	5.0.77

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `modovista`
--

DROP TABLE IF EXISTS `modovista`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `modovista` (
  `IDMODOVISTA` int(11) NOT NULL auto_increment,
  `DESCRIPCION` varchar(30) NOT NULL,
  PRIMARY KEY  (`IDMODOVISTA`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `modovista`
--

LOCK TABLES `modovista` WRITE;
/*!40000 ALTER TABLE `modovista` DISABLE KEYS */;
INSERT INTO `modovista` VALUES (1,'iframe'),(2,'window');
/*!40000 ALTER TABLE `modovista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_programa`
--

DROP TABLE IF EXISTS `modulo_programa`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `modulo_programa` (
  `MODULO` varchar(20) NOT NULL,
  `PROGRAMA` varchar(20) NOT NULL,
  `RUTA` varchar(100) NOT NULL,
  `IDMODOVISTA` int(11) NOT NULL,
  PRIMARY KEY  (`MODULO`,`PROGRAMA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `modulo_programa`
--

LOCK TABLES `modulo_programa` WRITE;
/*!40000 ALTER TABLE `modulo_programa` DISABLE KEYS */;
INSERT INTO `modulo_programa` VALUES ('CATALOGO','USUARIO','../catalogos/usuarios/',1),('CATALOGO','CLIENTES','',1);
/*!40000 ALTER TABLE `modulo_programa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `productos` (
  `IDPRODUCTO` int(11) NOT NULL auto_increment,
  `DESCRIPCION` varchar(100) NOT NULL,
  `PRECIO` decimal(9,2) NOT NULL,
  `VIDEO` varchar(200) NOT NULL,
  `VIGENCIA` varchar(50) NOT NULL,
  `FABRICANTE` varchar(50) NOT NULL,
  `IDIOMA` varchar(50) NOT NULL,
  `EDAD` varchar(100) NOT NULL,
  `MIME` varchar(50) NOT NULL,
  `FOTO` blob NOT NULL,
  PRIMARY KEY  (`IDPRODUCTO`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Alan Wake','199.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/auw3_z9EyRg\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Microsoft','Multilenguaje','Mayores de 13 años','',''),(2,'Batman Arkman City','299.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/-V1ZF5cNYCs\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','2k Games','Multilenguaje','Mayores de 18 años','image/jpeg','����\0JFIF\0,,\0\0��\0Created with GIMP��\0C\0	\Z!\Z\"$\"$��\0C��\0\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0\0\0	��\0G\0\r\0\0\0!1AQ\"aq��#2BRbr�����47u���3e���$%Dc����\0\0\0\0\0\0\0\0\0\0\0\0\0��\0)\0\0\0\0\0\0\0\0!Q1Aa��2q����\0\0\0?\0�R�\nR�\nR�\nR�-O}z��!��ղ�Hp$�>��?\n�@�\'�ݛ��%ǌ�҄�p`�\"��Y�F��*}�Y:�hyV��gj�@Ȍ��X�7\'S_�H�a>*�Hǻ�g�{N����X-7��y7��GD��[�.�ic w���[r\\uyN졅�m��v�՞����,��Iobw�A����±��$Nu�E�(\\t \";AA*IP)on�|�_[�ե���w�^m1)��r6aջ���6��\'��p+�w{s�:�$���B��@{�`��~���6��%\nB�l��N;3�ɫF-��P��^���C\n��{�za ��UO���N7�LN�|�\0�u�(�u��T��%\r)J	� Sr�mAO�Ԥ��ehmJJB�03�K��p)��6� ����W9A�����\n-�Lxrz<��u\r��h+�bS�c��9��]8��w�e�7����=�\Z�o����~QU۵�Z��T���\n��(����۷�|Ԩr���H9>=�<`s�s�\n�Y\r�����.u&_�-v����?y?��_4�N��qlht��#Z�\ZE��?-�C�0?�e�\'v�bW��ז9j!����O�Y�R�Z�\0dן��R�����#x�5->�������*��+��Ou��ߕFK�`�*�{~��no�o��(dn?��虴�51�b�꒲��I@�\0���8�ix�]A#�ֵ�m���T�\0��Ӕ7\Za���\"��(s\\�W�B���K���,���NiJWҝb�������������-A*VpNk�(1�Ŷs%��%��t����*���M�;aEK\0�c*�YJqUŻ�m��\0O�j�����\0��5J��F-��ɺ��#�����)��\0O�j��sJ�[�ߑm��\0O�j����������\\�W�\Z����0����vG�����B�=��-��Ȑ�dU��_Ҿ�k�����j�k��N��mw-P����u���2�<���Z�3������=?�>(����R�r[���\\c?\Zb����:�i�����κRBR����\\���.\\\'\\���%-jRПY8�+�iZ\\W�?��|WL��q�\"h�������%JyN8²9���7�\'�\\����T�W��3]UJַ(��<I��ОE��4�N�W����o	�\\x\0ª�N�\rG�~�=.��Ņd\n����J�Z����G�Ʃ~K[�k�;���4t��N�������l����w2�O�S~��=:�k�>�F6�ˈ)\n�r4}�~�y���c$�_�R؍)��ek.-�R�u���῎ihqU�b�3�Ɩ�i�)�)^���)@�)@�)@�)@�)@�)@���R�}���.ۢo3�d���q�x{𠓃���A|�`M}S�\\��:�֘�)�5��W���\0��jP�8���8?\n��ΰ��,���&O��r[��Y������~�\"R�U�����R�o[KP\Z�|�\"L�qR2�����A�+�t����kRn��{�EpV���}\r�\"�f�ۅV�h<ϙ\Z\'�Ku,�a�8�<%)$�B�Vk)�m�K-յgD�\Z���V�݂��G�O��R/��Xn�5<��i���k�����V!�3���dy���I�-N�+7�S����׽�p(m*��8��4��뾦İ{�\Z\'Hxi�ӭ����\\��G���]�m%���i��k&o���)IZR��J^��!p%X4�_��)JP)JP*��ҟ�W�R���	�EAR����������������Z��]2ԓG��m�,���*QPκ������ �\0�4٧nև�n�VU!�6�ds��RTvNN8�Sn���jN�\\\\���[�PF	<�z21\\�PL�_�~�;�O������\n�ܝ4\"Ɛz�$��� ����r09�B�i]u���\n�9ܤ͕$�@|S�\0H����7J�o�f��F�n��Z.$�hR{�C�kd�-�Uin�ݽ���l;��q�M���#���V���+�÷<P�R����v�	���$wҝQ�\'¿OV6Il?H��ƹ*RfA}�`������mt/J>�zCWiK�ۺSc�Y��L�n�)Z9SJ���w���SR�\')<V.���s�6J22p{��T��ԗ)\Z�UͼLQS�	nIt��c�י�ޤ�#�U�k��JV���yVc���Cp[��<w8��D�\\՞�\0n�L��J���C��s�(��;��ٗ%%��$�:���]5:�����x��xJ­�^\Z����y�Wvh�n1�ݾ�`�%��x �z��8��j2c	�7wW����#�HR��R��U3��?���*�S?����\"�EJR�!JR�JR�JR�JR�JR�JR�Po�	#�\ZЎ��\'�3S��} y膴�i?��D����|b����QY���إ�I2\"n\0��~)�����v|��x�\0(�6�&YiYH\0\0G���]���-Oyar~��ͩ-:�r�9��q�p<���s��!���e[h�puJL���B�XI��{dy�\ZѬuDk\r�/xoٜ�^s���>���*C���~Q*<V��R��}���19ljq6�<8�c�:��RZPSd��VwƜrf�d�@kgk}-dn��#>	@�W��\Z��2W���S����Y\Z�1\"xl4x9��T.L���I�b����J�]��B�+_#̓]�`�~������-�bp��_F�����S鞝���i�C/Z�y\"Qi�49 ���O�uu�1c�;a\r6P��=*����ӎ-�5��jpa[T�1��>��us���\'QdҶ�������ҖЬ��V��Jj!33-�)��r�o�w��uN�η�H ��a���Y\n\n��\01�\'����AJ�۵͖n��Y���{!�V�\0?AC��?]d�]�>��0�PĤ� �\n#��3�ޕ�֥��k��LT4%�+�P@�������xa�n.���|�q;�\02�����H�3��?���*�O_-��B]�J]Oi��g\nF����������R��HR��R��R��R��R��R��T��ܖ��\0�����A�����/�i?��K�Ir��r����Va�1-.������dx7����\0(�͖ee@g�����d��db�&[\r@�oJ�\n�?]Y�\\�#KR3�# �_�R��B�ʎ<�/m9r�f�QN��?֧l��~��bԺ�پ�=�G}\n�-0��Ȓq�Q��v�*(��XP�r<ϭl��kX�~�H�-��ԐR��~�΃9?Hi�jjd\'�������1[��RYV�a�����*�E~~鞢\\�Z\Z��*����\\*�p|�]u�rձ�v��R�+�?U�i����U���.7���?�|W4���\Z��k�훛ҟL��\0�KQJ֓������b�k^toI�]@����}��P���(H�m`���&2D/�G-8ԕ{����rf��]�}d�J�>�g¡	�2�i�U㤤����X���~��Pf���� ϙ�!�M���J#(P�������Mmg�#�z�^��]i�)J\n9ܣ每�R|;ŗT�Π�Ϸ�*��r�!�%\n�����xQ ��M�N�kXj����d��&\"O)zNr�TA�A��8��ӎ�j�6�E��ݹLǈ����k[�ԅ-M����V��3�Ɖ*lG�)Wl��+I�Q��dh�r5��m���M�Yi-X.�����)@s�	\\�s|�-��w�V\'L1U����#rS�p��xl��?kb5ѫ�W�=5MO!y[I-���q]���fn�8�2ᓥzG��$�nc�RrI\'�Z�\Z�vFt�C�r�-�����B7�$pq�$g4\Z����N�O[�q�rp�T�ݹ��`����WM!iq�֓���쨼���\r����Jv�jq��1Rf\ZC�Cm�z�REzR�\nR�\nR�\nR�\nR�\nR�\nR�\n���g���z��!��B���E���\0���CA��y�vq9�;�VV�e���D�6:O�@�Շ�/W~I�QY������mҁ�n�t\r��f7�R����+u�|F>*H�>#����}���e�2	�&�O���ٍ�֑	IW�&8�#�>���J��\Z=�iki�RT���G@�g���́�7�a8C��0sޱ:�Pƚǳ�a�YW?P��I�v����(3�L����@�09V0I��O:W���V�s�,nb�*��&��d!\r�8+Q�G�u��pu6�ܥxL��;P\n�~}�:G��HӺ�M�\"�0\"S\r�\"���q�1�I��m[�x��z�M=ӫn�ײngI���TVw��۱����T���x�\'!��g��\"L|�p��\n�{m�?�U��ɧH��g\r��o�K��۪K��\'�T���?z�;\n��9�IIK�H��P�we��-0Yiq���yj`𖓀Rs��y�����B4�pnK���fA� `%d�223ȪN������ԋ��g;k���,�D0����d6�}Jϳ%@�~j8���Ɣ�z:�ͪʘ�!N�>j\'��6����۝�U����Hz*���S��1��P�8>U��	p�p^,���Vcm��$\'k඼\'�ǩ�5.����i�����T�h8��2R3�P�d8[����zֵn]�����Z�w�8�R!��\0�(���v�n;�ΰ�M��X�G,�S��(9��,�<q�t��E#W1��xH����3��	���Ф�\'� ��\0hO�+���h��z�j��M�$t2Kn�J�F28,���=�O��\'1���\"�����R�e\nR�\nR�\nR�\nR�\nR�\nR�\n�u��WX��g������@�GY��4����F�����$�?�*��Aw��Qڔ�K�e��@�0����	;_mI��Pn��q��K�*Pu���[��)��,�y�w��I�9}Ci�\'*�J�ܯXm[��	�l(�7�����,	�����=j�J!��Z��[c�*	������=2§ܒ1���]��}r�O����\n�=\'p��0�A#vk�>��nz�t��?��x\'�����\0||�6�5�RT����k;oy��H ��*9&3�1�mW�^�0�e���*	�rR�)���j*r�]i�m[b�\Z=�b�i��(qI!����哞D�PF@]Im�ġ+X Uf���\Z���bve������K�n�%R]Z�V�g�\0�UKTmn�u�r\\��wK�\'��\0�H�z�հ��%~�@8��D-:ל�|�Ԝ�%�U��qrd4�W��;RI��#Q���E$��{�\\0�G8�����������­��V���6���!��:JS�*e��[�VNp\Z8�EA��QKo)@��\'��[Ťۡ~rp<��Ԩ�Ҕ�R��R��R��R��R��R��T����?�i?障���\0܎��\0�����ľ���/����ŐB�#�o��Z��/dy\'�EY%�$�	�W��	���x��bJO���\Z��eV�d� 69Wr@��~����P�C�8P<f��.�5�ثWt��^���۱%!M�p�4�k^��$���ϭL�/�E�d5�y);jsAC�h�GcAw���ɱ��Cju-���98����h�_��c[ZS�|Wlq�H�=��5½3J>�!FB�1��?~+��]�ӝ0T���\Z�I��\n6�<�=;�x��L�)-� ���5�Ӱ��e/\nBKdd�ֱ�^]Ň���\n���9��v��������i*V<�=Q�bƔ����j��x�&�V�J;�W䚊j�G.}�*� H)Zq���^4��(�c% �Nn�{��ʻs-���Y�|*�W@����#Q�{��\0prJ�ݕn��)PEo�Oi��e�\0)�欵���{\\Xo2����R6�9�Ʋ������9RӃ�s�j\'uT�R����Q���5���\0�@��N�\Zۿ}�<�!��4�2�jt�Sn3��+�Ҷ\'H����Q$s��\0i��4�(�(�(�(�(�(�(\r����[�Jݴ���2I(52�_]],tkW�\0%�\n��A�����{sۣ-��@mGV=<�Al�$���T&5��M��Q���BO���]F:��/:��[���rV�ͨ)+c��P`R���몌Iq�2H�rWq���kx�{��A��1M>\\B�WqT�*�A$�­\"�	��*��s~\\7��bJ�l)���������Km�Z�\Z��q(Z5��WFu�P���N\n�\0��]ڬ����O�*�Nzc\Z�n��UT�O�i(!	� �d�U��P\\V$+QY$,\'n|u�,��\ZwVaM�3�$�KkXڥ\'��*U�/%�AYZR��g8�uY��Z�)���TT2�8�2����~���[n�[���Ǖ{T�p0%4d�\nI�}��Uz~\Z��uc�m��\01Q�ޛ��mr&�T�#��Q���\0x9\'5\0U��(�,W�B��J���[,�CE_2;yA��{;w�` ��¦�N�Umm�^��p�c�}+���b��)�	�wm:��8�7,��\0���\0nk�m\\�]t�\rShSsl�C�J@r+�e��}�S]PH��T����8��d��(����S�m�j�x�5�k{+Z�J��XKݭ�:��yq�����S���=�T��\0,s�\Z��7R.*�1��%�c�֧�͗H~��x�p{[�יmT�\0���Y�G��YZ9ǯ6���������������|I_E5�R	&�$\0?P�ޣ�MCN������РGm4��r��K�Ó�%!8�h��sQ����R�q�D\n!ǈ$��;�J��+S�#3]��An�*�P�������uANnQCЮ	g�J����)���c����S����2����^���.z;dlNs]\rscKi�&B԰�+-�l8�G#=�\0����V0k���`��9�)h��A��N��l��йڮ�G�����ʏ\"�k;��~晷�|�ckQ-!#\r�\'�zq[&wV�����<,�(i�;��#\'֠�M�S:���H[�u\'v�mJs�h7)�O[���neC�F��m�nڤ�$��X�\r_�2��(�o����p6�)���y�9I�{��D�M>��*dY���V#�`�qU��[���l�[\\�졧���aK������}��^�̧�d8�y��|ja`�������\'����{׫����4�Z9J���\0eH��wP���.3;\'w�I�$�\r�\'�s�E|K�@HiGӜ�JПJ}\'LɉO\\�-�\0��G��;�)V�ީ����Ђ=�g@HJrI>u�\\y�ב:�9���(�eHqK-��{\Zĩs5�5�U���z20��#�\0�8��<��Z��C+FZ���v�74���+�V��B�TkV�N�ⷸ���s�y�\0z���;NO�b�~�.�먮5f��X��N��	*#<dd�W���pܫ�8*;gzm\n���=�\\=u�z�\r�ݨ�X5-������;���aH��꿢�UW�4�:z�|i7�JNR�7\r.H���|h>�)b薖0�<R���:�o���Yφa�<cZ?�P�YL=Srb@B\\����}F�7I풭��Q��$=nܒNs����mZR�\nR�\nR�\nR�\nR�\nR�\nR�\n����#tkW�h�m�$)\'�jiP����z+�Zi%KU�HHg�4s�5��z���G�Y`$�h���.��iNG�7g���a�/���ۗ�A��o����<a{ty-��HI\0mq;B���Z}e٣\\5�DD+���+)ov´7�@`����~�ңo�Ϊ�]&ɋff�=��->�:\\e�s�(m9��L��Hj�T]5e�K.�����C�!BT�P���!8�U3M]��{:�ۑ��\0�]��ז�z��2v�!�F�z���Ѯ�n�-���ܟO���pΒ���(���#޳��\\�\';\\�����=U��HK�#$rB��q�J-�=&��5,#�l�����]xG�1��\0��>����b�ZO�FPx>�xeOa�\n,B��&(��j)\0�=�V:��/Ӧ��^�U�L����#\'ꠑj�,3�š��f,%�Z�8�D�F���j)�\r3w����\"���n�`!����w\0��FA<�\r�-Gu��.Z�]��\"�o!ּ�����R�.�T���j\r<�@��\r��&\"O��z�VФ�\\��T1��j�#���x.����)m8�I��E�p�^o��-�-��i\0`����ʶu�DZ�JzSr�}�8��;�>�\0�>Y�m�3�m�D�����()D��ʲ8���z)ӽh�2�oF��8���H$����<`|rkԽ��]���$[�	>�&3a��%<)>��*C�.6�m�o� oK��J��8*�6����Q�O�!��1\"ICi-EJ�V��y�	\0c4>�u\']�]Zވ����Sd!L���gOd���(����Ϊr=�]}��V�u�����M�!�E\0m `��yI�\Z�F�}���ı��ZG������yZ�����q\\��\\\'VN!�)<)+\'n�ǘ<���Kn7�D�П�˻>䖰��RR�\r��)\0l;��21�Em?���e�Y]��*z֘i�Pގw�>X�wk���f�Q���\r����T�7�\n�	;2@�]A�^�Jї�WSP�����\0ZN�����T�(�(�(�(�(�(�(���e�?Þ�SJPp���\"�n��\r��̆䍿����A&ucW�RXS�o2�����($y��}��,��7�c1�w�\Zu��hF�A�~j9JQ�R�}��5y�=�xR�O�F�����צ��9���-i�Y?����8�)Az>φ�ͺ��I#?mY?�.d7��Im;PR��#� Ҕ�5ΣDQX	�lw*���z�l�~��K@`!.��f������R�T�̹��?�a灟�Y��,ښ,�Zn#c��$���)A�*B�V��}��!8��<R���)\n�%>�\0<WD}.W	������d!�R�]*\0���JPu�)J)J)J)J��'),(3,'Battleflied 3','299.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/UIUJh2mA8vg\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Electronic Arts','Multilenguaje','Mayores de 18 años','',''),(4,'Gears Of Wars 3','299.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/kFmYbaAjrfo\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Epic Games','Multilenguaje','Mayores de 18 años','',''),(5,'Halo Reach','299.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/5iWyoxv_V8o\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Microsoft','Multilenguaje','Mayores de 15 años','',''),(6,'Uncharted','269.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/l4fahAdNHpM\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','NautiDogs','Multilenguaje','Mayores de 15 años','',''),(7,'Resident Evil 5','186.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/Lxun_JOiiXo\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Capcom','Multilenguaje','Mayores de 15 años','',''),(8,'Kirby Dream Land','269.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/3YXN_0R_8WY\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Nintendo','Multilenguaje','Todo Publico','',''),(9,'Zelda: Skyward Sword','299.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/UOMRs6Jkgo0\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Nintendo','Multilenguaje','Todo Publico','',''),(10,'Final Fantasy Crisis Core','189.00','<iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/DKp82VAUbh4\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Square','Multilenguaje','Todo Publico','',''),(11,'Super Mario 3D Land','225.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/1c4xHskYHns\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Nintendo','Multilenguaje','Todo Publico','',''),(12,'StarCraft 2','220.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/rgyL08nhtkw\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Blizzard','Multilenguaje','Mayores de 15 años','',''),(13,'Skyrim','220.00','<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/PjqsYzBrP-M\" frameborder=\"0\" allowfullscreen></iframe>','Disponible','Betesda','Multilenguaje','Mayores de 18 años','','');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuario` (
  `IDUSUARIO` varchar(20) NOT NULL,
  `CLAVE` varchar(40) NOT NULL,
  `NOMBRE` varchar(50) NOT NULL,
  `APELLIDOS` varchar(50) NOT NULL,
  `ACTIVO` tinyint(4) NOT NULL,
  PRIMARY KEY  (`IDUSUARIO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('fsanchez','8cb2237d0679ca88db6464eac60da96345513964','Frank','Sànchez Moreyra',1),('admin','8cb2237d0679ca88db6464eac60da96345513964','Administrador','',1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-09-07  3:32:15
