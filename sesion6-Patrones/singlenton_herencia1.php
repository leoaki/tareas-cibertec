<?php 
include_once 'config.php';

class Conexion extends PDO
{
    public $dbh;

	 public function conectar() {
    	
                $this->dbh =  new PDO( DB_ENGINE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
      
        return $this->dbh;
    }
    
    public function socket_conexion(){
    	
    	 return $this->dbh;
    }
    
}


class Alumno extends Conexion
{
	public function __construct(){
		return parent::conectar();
		
	}
	
	
}

class Curso extends Conexion
{
	public function __construct(){
		return parent::conectar();
		
	}
	
}

/* PROGRAMA PRINCIPAL */ 
$AlumnoPhp = new Alumno();
$CursoPhp = new Curso();

if ($AlumnoPhp->socket_conexion() === $CursoPhp->socket_conexion()) echo 'Manejan la misma conexion';
else echo 'Las conexiones son diferentes';
    
?>


