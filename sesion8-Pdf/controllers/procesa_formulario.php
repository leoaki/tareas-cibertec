<?php
require_once '../modelo/autoload.php';
$datos['IDPRODUCTO'] = $_POST['IDPRODUCTO'];
$datos['DESCRIPCION'] = $_POST['DESCRIPCION'];
$datos['PRECIO'] = $_POST['PRECIO'];
$datos['VIDEO'] = addslashes($_POST['VIDEO']);
$datos['VIGENCIA'] = $_POST['VIGENCIA'];
$datos['FABRICANTE'] = $_POST['FABRICANTE'];
$datos['IDIOMA'] = $_POST['IDIOMA'];
$datos['EDAD'] = $_POST['EDAD'];
$datos['MIME']='';
$datos['FOTO']='';

$msg= '';
if (isset($_FILES['foto'])){
	$nombrearchivo= basename($_FILES['foto']['name']);
	$destino_archivo = "../../temp/".$nombrearchivo ;
	
	if(move_uploaded_file($_FILES['foto']['tmp_name'],$destino_archivo))
	{
		/* array de tipos de permitidos */
		$mimes_permitidos = array('image/jpeg','image/jpg','image/png');
		$datos['MIME'] = $_FILES['foto']['type'];

		if (in_array($datos['MIME'], $mimes_permitidos))
		{
			$fp = fopen($destino_archivo,"rb");
			$contenido = fread($fp, filesize($destino_archivo));
			$datos['FOTO'] = addslashes($contenido);
			fclose($fp);
		}
		else{
		$msg = "Archivo no tiene el formato adecuado<br>";	
		}
		//unlink($destino_archivo);
	}
	else
	$msg = "La carpeta Temp no tiene derechos <br>";
}
else 
   $msg = "No subio ningun Archivo.<br>";

$prod = new Producto;
$prod->setProducto($datos);
echo $msg;
echo "<a href='../vista/form_producto.php?idproducto=".$datos['IDPRODUCTO']."'>Retornar al formulario</a>";
?>
