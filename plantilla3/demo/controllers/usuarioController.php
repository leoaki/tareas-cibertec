<?php

class usuarioController extends Controller
{
	public function __construct() {
		parent::__construct();
		session_start();
	}
	
	public function index()
	{
			
	}
	
	public function addUsuario(){
	// recepcion de datos del formulario  y limpieza de los espacios en blanco
	$usuario = trim($_POST['usuario']);
	$nombre = trim($_POST['nombre']);
	$contrasenia = trim($_POST['contrasenia']);
	
	// carga el modelo y llama al metodo para insertar un usuario
	$this->_usuario = $this->loadModel('usuario');
	$result = $this->_usuario->setUsuario($usuario,$nombre,$contrasenia);

	// validacion del error al grabar
	if ($result)  $this->_view->error ='Error al registrar usuario'; 	
	
	$this->_view->renderizar('index');
	}
	

}