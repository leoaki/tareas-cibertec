<?php

//  clase de conexion a la BD utilizando patron Singleton
class Database extends PDO
{
	private static $instancia ;
	public function __construct(){
		
		if (!isset(self::$instancia)) {
			
			$dsn=DB_ENGINE.':host='.DB_HOST.';dbname='.DB_NAME;
			
			try{
				self::$instancia = parent::__construct($dsn,DB_USER,DB_PASS,
				array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . DB_CHAR));
			
			}catch(PDOException  $e){
				self::$instancia=null;
				echo "Error de conexion con  la bd ".$e->getCode();
			}
		}
		
		return self::$instancia;
	}

}

?>

