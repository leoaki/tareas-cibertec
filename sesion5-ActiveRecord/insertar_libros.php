<?php
require_once 'config.php';
require_once __DIR__. '/php-activerecord/ActiveRecord.php';

class Libro extends ActiveRecord\Model { }


$cfg = ActiveRecord\Config::instance();
$cfg->set_model_directory('.');
//$cfg->set_connections(array('development' => 'mysql://userlibreria:12345@localhost/libreria'));
$cfg->set_connections(array('development' => DB_ENGINE.'://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME));


$libro1 = new Libro(array('nombre' => 'Los jefes', 'autor_id' => 1, 'precio' => 45,'moneda'=>'USD'));
$libro1->save();

$libro2 = Libro::create(array('nombre' => 'La vida exagerada de Mart�n Roma�a', 'autor_id' => 3, 'precio' => 35,'moneda'=>'USD'));

echo 'registros grabados';


?>
