<? 
include_once 'Config.php';

class Usuario extends mysqli
{
	public function __construct() {
		parent::__construct(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if ($this->connect_errno)	die('Connect Error: ' . $this->connect_errno);
		return;
	}
	
	public function insertReg($idusuario,$nombre,$contrasenia,$activo,$rol){
		$sql="insert into usuarios set IDUSUARIO ='$idusuario', 
										NOMBRE ='$nombre',
										CONTRASENIA = SHA1('$contrasenia'), 
										ACTIVO='$activo', 
										ROL='ADMINISTRADOR', 
										FECHACREACION= NOW()";
		$this->query($sql);
		return $this->errno;
	}
	
	public function updateReg($idusuario,$nombre,$contrasenia,$activo,$rol){
		$sql="update usuarios set  NOMBRE ='$nombre', CONTRASENIA = SHA1('$contrasenia'), ACTIVO='$activo', ROL='ADMINISTRADOR' where IDUSUARIO ='$idusuario'";
		$this->query($sql);
		return $this->errno;
	}

	public function deleteReg($idusuario){
		$sql="delete from  usuarios where IDUSUARIO ='$idusuario'";
		$this->query($sql);
		return $this->errno;
	}
	
	public function getAllReg(){
		$sql="select * from  usuarios";
		return  $this->query($sql);
	}
	
	public function getIdReg($idusuario){
		$sql="select * from  usuarios where IDUSUARIO ='$idusuario'";
		return  $this->query($sql);
	}
}
//instanciar el objeto
$registro = new Usuario();
$result=$registro->getAllReg();
?>
<html>
<table border="1">
<tr>
	<th>Id Usuario</th>
	<th>Nombre</th>
	<th>Contrase&ntilde;a</th>
	<th>ACTIVO</th>
	<th>ROL</th>
	<th>F. CREACION</th>
	<th>F. MODIFICACION</th>
</tr>
<? while ($reg=$result->fetch_object()):?>
<tr>
	<th><?=$reg->IDUSUARIO?></th>
	<th><?=$reg->NOMBRE?></th>
	<th><?=$reg->CONTRASENIA?></th>
	<th><?=$reg->ACTIVO?'ACTIVO':'INACTIVO'?></th>
	<th><?=$reg->ROL?></th>
	<th><?=$reg->FECHACREACION?></th>
	<th><?=$reg->FECHAMODIFICACION?></th>
</tr>
<? endwhile;?>
</table>
</html>